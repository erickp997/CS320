/*
* Erick Pulido
* Dr. Carroll
* CS320
* Perform specific tasks depending on character input.
* February 21 2018 (February 16 for extra credit)
*
* Program 1: Reads in characters and performs following tasks:
*   	    1) Print two spaces for every space
*   	    2) Does not print any numbers
*   	    3) Reverses case of letters
*   	    4) Does not print multiple consecutive lines
*   	    5) Repeats anything else verbatim
*/
#include <stdio.h>
int main(void)
{

    int iochar,		// collect input one character at a time
    consecutive = 0,	// count consecutive newlines
    modCounter= 0, 	// count changes to input, 1 or more returns 0
    changeCase = 32,	// difference between lowercase and uppercase
    tooManyLines = 2;	// threshold to determine multiple consecutive newlines

    //  template from L.S. Foster's C by Discovery
    while ((iochar = getchar()) != EOF) { 
	
	// streak of consecutive lines is broken
	if(iochar != '\n') consecutive = 0;
	
	// single space becomes two spaces
	if(iochar == ' ') {
	    putchar(iochar);
	    putchar(iochar);
	    modCounter++;
	}
	
	// numbers are simply omitted 
	else if(('0' <= iochar) && (iochar <= '9')) modCounter++;
	
	// lowercase letters become uppercase, difference between them is 32
	else if(('a' <= iochar) && (iochar <= 'z')) {
	    putchar(iochar - changeCase);
	    modCounter++;
	}
	
	// uppercase letters become lowercase, difference between them is 32
	else if(('A' <= iochar) && (iochar <= 'Z')) {
	    putchar(iochar + changeCase);
	    modCounter++;
	}
	
	// multiple consecutive newlines are not printed, single lines are
	else if(iochar == '\n') {
	    consecutive++;
	    if(consecutive <= tooManyLines) putchar(iochar);
	    else modCounter++; // when lines are ommitted, that is a change
	}
	
	// symbols are repeated verbatim
	else putchar(iochar);
	
    }
    
    if(modCounter > 0) return 0;    // modifications were made
    else return 1;  	    	    // no modifications were made
}

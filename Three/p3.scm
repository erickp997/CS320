;this is the prototype for p3.scm (cs320 Program 3, Spring 2018)
;EDIT the next few comments to add header info (name, instructor, purpose, etc.)
; Erick Pulido
; Dr. Carroll
; Define the following four methods through functional programming.
; 4/11/18 (Extra Credit) 4/18/18 (Normal)
;apart from adding the above header comments, leave this file section UNCHANGED
;To run this file, you would type (from the '%' prompt in the proper directory):
; scheme --load p3.scm
(load "~cs320/Scheme/simply.scm")
;Here I define a few handy lists that we can manipulate:
(DEFINE list0 (LIST 'j 'k 'l 'm 'n 'o 'j) )
(DEFINE list1 (LIST 'a 'b 'c 'd 'e 'f 'g) )
(DEFINE list2 (LIST 's 't 'u 'v 'w 'x 'y 'z) )
(DEFINE list3 (LIST 'j 'k 'l 'm 'l 'k 'j) )
(DEFINE list4 (LIST 'n 'o 'p 'q 'q 'p 'o 'n) )
(DEFINE list5 '((a b) c (d e d) c (a b)) )
(DEFINE list6 '((h i) (j k) l (m n)) )
(DEFINE list7 '(f (a b) c (d e d) (b a) f) )
;here is a typical function definition (from Page 681 of Sebesta):
(DEFINE (adder lis)
  (COND
    ((NULL? lis) 0)
    (ELSE (+ (CAR lis) (adder (CDR lis))))
))
;the above five lines are the sort of definition you would need to add to
;this file if you were asked to:
;"Create a function that accepts a single list as a parameter and returns
;the sum of the [numerical] atoms in the list.  To do this, uncomment the
;next DEFINE and modify it to conform to the specs."
; (DEFINE (adder ...complete this definition
; 
; 'adder' has already been defined for you (see above), but you must create
; the following four definitions [see ~cs320/program3 for full details]
; So, uncomment the next four DEFINEs, and modify them to conform to the
; specs given in the ~cs320/program3 writeup.

(DEFINE (evens lis)
  (COND
    ((NOT (LIST? lis)) "USAGE: (evens [list])")  
    ((NULL? lis) '())	    	; Test for empty list.
    ((NULL? (CDR lis)) '()) 	; From Page 671 of Sebesta, test for 1 item.
    (ELSE 			; Get second item in list, move down list by two.
    	(CONS (CADR lis)  
    	(evens (CDDR lis)))))) 
    
(DEFINE (oddrev lis)
  (COND
    ((NOT (LIST? lis)) "USAGE: (oddrev [list])")
    ((NULL? lis) lis)
    ((NULL? (CDR lis)) lis) 	; Single item is already 'reversed'.
    (ELSE 		        ; Recursively move down by two on a list [odds].
    	(APPEND            	; Build a list of first elements of the CDDR.
	(oddrev (CDDR lis)) 	; List 'reverses' by appending the next odd element
	(LIST (CAR lis))))))	; in front of the previous odd.

(DEFINE (middle lis)
  (COND
   ((NOT (LIST? lis)) "USAGE: (middle [list])")
   ((NULL? lis) '())		; Middle of empty list is an empty list.
   ((NULL? (CDR lis)) lis)	; The item in a one item list is the middle.
   (ELSE 			; Recursively cut off ends until middle
   	(middle
	(CDR 			; element is reached. Should handle
	(REVERSE (CDR lis))))))) ; even numbered lists as well.
   
(DEFINE (endsmatch lis)
  (COND
   ((NOT (LIST? lis)) "USAGE: (endsmatch [list])")
   ((NULL? lis) #t)	       ; Empty list by default has 'ends match'.
   ((NULL? (CDR lis)) #t)      ; One item list also has 'ends match'.
   ((EQUAL? (CAR lis) (getlast lis)) #t)
   (ELSE #f)))
   
; Helper function for endsmatch
; Cycles through list recursively until the last item is left.
; Along with endsmatch also handles nested lists.
(DEFINE (getlast lis)
  (COND
   ((NULL? (CDR lis)) (CAR lis))
   (ELSE (getlast (CDR lis)))))
   	

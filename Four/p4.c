/*
* Erick Pulido
* Dr. Carroll
* CS320
* Perform specific tasks depending on character input.
* April 30 2018 (April 27 for extra credit)
*
* Program 4: Reads in characters and performs following tasks:
* 1) Print two spaces for every space
* 2) Does not print any numbers
* 3) Reverses case of letters
* 4) Deletes all but first of duplicate consecutve lines
* 5) Repeats anything else verbatim
*
* Behavior Notes:
* 1) Does not always return correct return value when multiple
* files are read in.
* 2) Program does not handle long lines.
*
* Returns:
* 0 - at least one change to input
* 1 - no changes made to input
* 2 - nonexistance/undreadable file
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants declarations */
#define	MAXCHAR		79
#define CHANGECASE 	32
#define ERRFILE		2

/* Global variables for easy work with methods */
char head[MAXCHAR * 2]; // declaration from CS320 Program 4 Notes
char tail[MAXCHAR * 2];

/* Determine whether lines are unique or not and print results */
int printResults(char *one, char *two, int modc) {
    if(!(strcmp(one, two) == 0)) // two is new or tail
    	printf("%s",two);
    else modc++; // duplicate lines were omitted, counts as modification
    return modc;
}

/* Reads in the values, returns modCounter to determine return status */
int readInput(FILE *ifp) {
    int iochar;
    int modCounter = 0;
    int i = 0;
    
    char *old = head; //arr1
    char *new = tail; //arr2
    char *temp; // needed to perform swap
    
    // borrowed from C by Discovery by Foster and my p1.c
    while ((iochar = getc(ifp)) != EOF) { 
	
	//end of a line, tests for duplicate line
	if(iochar == '\n') {
	    *(new+i++) = iochar; // place a newline in the array
	    *(new+i++) = '\0'; // null terminate the array
	    modCounter = printResults(old, new, modCounter);
	    i = 0; // reset indexing
	    temp = new; // perform swap needed to read in
	    new = old;  // from multiple lines
	    old = temp;   
	}
	
	// single space becomes two spaces
	else if(iochar == ' ') {
	    *(new+i++) = iochar;
	    *(new+i++) = iochar;
	    modCounter++;
	}
	
	// numbers are simply omitted 
	else if(('0' <= iochar) && (iochar <= '9')) modCounter++;
	
	// lowercase letters become uppercase, difference between them is 32
	else if(('a' <= iochar) && (iochar <= 'z')) {
	    *(new+i++) = iochar - CHANGECASE;
	    modCounter++;
	}
	
	// uppercase letters become lowercase, difference between them is 32
	else if(('A' <= iochar) && (iochar <= 'Z')) {
	    *(new+i++) = iochar + CHANGECASE;
	    modCounter++;
	}   
		
	// symbols are repeated verbatim
	else *(new+i++) = iochar;
	
    }
    
    return modCounter;
  
}

int main(int argc, char **argv) {
    FILE *input = stdin;	    	     

    int i;
    int mods = 0;
    int fileStatus = 0; // handling the return value for unreadable files
    
    if(argc > 1) { // read from file(s)
	/* Beginning of normal file tests */
	for(i = 1; i < argc; ++i) { // borrowed from CS320 Lecture Notes
	
	    input = fopen(*(argv+i), "r"); // read from the file
	    
	    if(input == NULL) { // can't open and read file
	    	fprintf(stderr, "Error opening %s file\n", *(argv+i));
		fileStatus = ERRFILE; // seperate return value
	    }
	    
	    else { // file can be opened and read
	    	mods = readInput(input);
		fclose(input); // always close file when finished
	    }
	}
	/* End of normal file tests */    
    }
    else mods = readInput(input); // read from standard input
    
    if(fileStatus == ERRFILE) return fileStatus; // could not open file(s)
    else if(mods > 0) return 0; // changes were made
    else return 1; // no changes were made
}
